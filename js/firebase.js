// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getAuth } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
apiKey: "AIzaSyDKN1J5Rw752gzyAi5U9d43MZSpB92Htis",
authDomain: "altapalta-31767.firebaseapp.com",
projectId: "altapalta-31767",
storageBucket: "altapalta-31767.appspot.com",
messagingSenderId: "772305449743",
appId: "1:772305449743:web:20508a17a5d8c1b8dec47a",
measurementId: "G-YJ8HY26YV3"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const autenticacion = getAuth(app);